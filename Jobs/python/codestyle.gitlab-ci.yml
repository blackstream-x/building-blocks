# ==============================================================================
# Python code style checking jobs
# ------------------------------------------------------------------------------
# include:
#   project: blackstream-x/building-blocks
#   file: /Jobs/python/codestyle.gitlab-ci.yml
# ------------------------------------------------------------------------------
# Stage: codestyle
# ------------------------------------------------------------------------------
# Required variables:
# * PYTHON_IMAGE → the Python container image
# ------------------------------------------------------------------------------
# Optional variables:
# * EXTRA_REQUIREMENTS → extra required packages defined at job level
# ==============================================================================


# ==============================================================================
# Job template: codestyle base
# ==============================================================================
.python-codestyle-base:
  extends: .python:native
  stage: codestyle
  variables:
    CODESTYLE_EXECUTABLE: echo
    CODESTYLE_CLI_OPTIONS: ''
    CODESTYLE_TARGET: '.'
  script:
    - ${CODESTYLE_EXECUTABLE} ${CODESTYLE_CLI_OPTIONS} ${CODESTYLE_TARGET}
  rules:
    - if: $CI_COMMIT_TAG == null


# ==============================================================================
# Check style using black
# This job does not fail but rather prints a suggestion
# ==============================================================================
python:black:
  extends: .python-codestyle-base
  variables:
    CODESTYLE_EXECUTABLE: black
    CODESTYLE_CLI_OPTIONS: '-l 79'
    REQUIREMENTS: black
  script:
    - black --check ${CODESTYLE_CLI_OPTIONS} ${CODESTYLE_TARGET} || black --diff ${CODESTYLE_CLI_OPTIONS} ${CODESTYLE_TARGET}
  rules:
    - when: never


# ==============================================================================
# Check style using flake8
# ==============================================================================
python:flake8:
  extends: .python-codestyle-base
  variables:
    CODESTYLE_EXECUTABLE: flake8
    CODESTYLE_CLI_OPTIONS: '--exclude venv'
    CODESTYLE_TARGET: ''
    REQUIREMENTS: flake8
  rules:
    - when: never


# ==============================================================================
# Type checks using mypy
# ==============================================================================
python:mypy:
  extends: .python-codestyle-base
  variables:
    CODESTYLE_EXECUTABLE: mypy
    REQUIREMENTS: '-r requirements.txt mypy'


# ==============================================================================
# Check using pylint
# ==============================================================================
python:pylint:
  extends: .python-codestyle-base
  variables:
    CODESTYLE_EXECUTABLE: pylint
    CODESTYLE_CLI_OPTIONS: '--disable=fixme'
    CODESTYLE_TARGET: 'src tests'
    REQUIREMENTS: '-r requirements.txt pylint'


# ==============================================================================
# Check style and lint using ruff
# ==============================================================================
python:ruff:
  extends: .python-codestyle-base
  variables:
    CODESTYLE_EXECUTABLE: ruff
    CODESTYLE_CLI_OPTIONS: ''
    REQUIREMENTS: ruff
  script:
    - ruff format --check ${CODESTYLE_CLI_OPTIONS} ${CODESTYLE_TARGET} || ruff format --diff ${CODESTYLE_CLI_OPTIONS} ${CODESTYLE_TARGET}
    - ruff check
